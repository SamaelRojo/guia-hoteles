$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    })

    $('#contacto').on('show.bs.modal', function (e) {
        $('#contactoBtn').prop('disabled', true);
        $('#contactoBtn').addClass('btn-background');
    })

    $('#contacto').on('hidden.bs.modal', function (e) {
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').removeClass('btn-background');
    })
});